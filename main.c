#include <stdint.h>
#include "defines.h"

//#define BLINKING
//#define BUTTONS
//#define TIMERS
//#define TIMERS_IR
#define ADC
unsigned long j = 0;
#ifdef TIMERS_IR
void TIMER0A_Handler(void){
			GPTMICR_TIMER0 |= (1<<0);
			GPIO_PORTF_DATA_W ^= (1<<2);
}
#endif
int main()
{
	//BLINKING LED
#ifdef BLINKING	
	SYSCTL_RCGCGPIO_R |= GPIO_PORTF_CLK_EN; //enable PORTF
	GPIO_PORTF_DEN_R |=0x0E; // enable pin3 on PORTF
	GPIO_PORTF_DIR_R |=0x0E; //set direction on pin3 as output
	while(1){
		GPIO_PORTF_DATA_W ^= 0x02;
		//GPIO_PORTF_DATA_W ^= GPIO_PORTF_PIN2;
		//GPIO_PORTF_DATA_W ^= GPIO_PORTF_PIN1; //change bit on pin3 - low to high, high to low
		for(j=0;j<DELAY_VALUE;j++);
	}
#endif
	
	//INPUT
#ifdef BUTTONS
	SYSCTL_RCGCGPIO_R = 0x20; //enable PORTF
	GPIO_PORTF_LOCK_R = 0x4C4F434B;
	GPIO_PORTF_CR_R = 0xff;
	GPIO_PORTF_DIR_R = 0x0E; //input for LEDs
	GPIO_PORTF_PUR_R =0x11; //enable pullups for switches
	GPIO_PORTF_DEN_R =0x1F; // enable 0b00011111 on PORTF
	while(1){
		switch(GPIO_PORTF_DATA_R & 0x11){
			case 0x00:
				GPIO_PORTF_DATA_W = 0b10;
			break;
			case 0x01:
				GPIO_PORTF_DATA_W = 0b100;
			break;
			case 0x10:
				GPIO_PORTF_DATA_W = 0b1000;
			break;
			default:
				GPIO_PORTF_DATA_W = 0b111110001;
			break;
		}
 }
#endif

	//TIMERS
#ifdef TIMERS
	SYSCTL_RCGCGPIO_R = 0x20; //enable PORTF
	GPIO_PORTF_DIR_R = 0x0E; //input for LEDs
	GPIO_PORTF_DEN_R =0x0E; // enable 0b00011111 on PORTF
	GPIO_PORTF_DATA_W = 0x00;
 
	SYSCTL_RCGCTIMER |= TIMER0_EN;
	GPTMCTL_TIMER0 &= ~(1<<0);
	GPTMCFG_TIMER0 = 0x00000000;
	GPTMTAMR_TIMER0 |= (0x2<<0);
	GPTMTAMR_TIMER0 &= ~(1<<4);
	GPTMTAILR_TIMER0 = 16000000;
	GPTMCTL_TIMER0 |= (1<<0);
	
	while(1){
		if((GPTMRIS_TIMER0 & 0x00000001) == 1){
			GPTMICR_TIMER0 |= (1<<0);
			GPIO_PORTF_DATA_W ^= (1<<2);
		}
	}
#endif

#ifdef TIMERS_IR
	SYSCTL_RCGCGPIO_R = 0x20; //enable PORTF
	GPIO_PORTF_DIR_R = 0x0E; //input for LEDs
	GPIO_PORTF_DEN_R =0x0E; // enable 0b00011111 on PORTF
	GPIO_PORTF_DATA_W = 0x00;
 
	SYSCTL_RCGCTIMER |= TIMER0_EN;
	GPTMCTL_TIMER0 &= ~(1<<0);
	GPTMCFG_TIMER0 = 0x00000000;
	GPTMTAMR_TIMER0 |= (0x2<<0);
	GPTMTAMR_TIMER0 &= ~(1<<4);
	GPTMTAILR_TIMER0 = 80000000;
	GPTMIMR_TIMER0 |= (1<<0);
	TIMER0_NVIC_ADD |= (1<<19);
	GPTMCTL_TIMER0 |= (1<<0);
	
	while(1);
#endif

#ifdef ADC
	
#endif
return 0;
}
