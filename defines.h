#define SYSCTL_RCGCGPIO_R (*((volatile unsigned long *) 0x400FE608))
#define GPIO_PORTF_CLK_EN  (1<<4)
#define GPIO_PORTE_CLK_EN (1<<5)

#define SYSCTL_RCGCTIMER (*((volatile unsigned long *) 0x400FE604))
#define TIMER0_EN (1<<0)

#define SYSCTL_RCGCADC (*((volatile unsigned long *) 0x400FE638))
#define ADC0_EN (1<<0)

#define GPIO_PORTF_DATA_W *(( volatile unsigned long*) 0x40025038)
#define GPIO_PORTF_DATA_R *(( volatile unsigned long*) 0x40025044)
	
#define GPIO_PORTF_DIR_R *(( volatile unsigned long*) 0x40025400)
#define GPIO_PORTF_DEN_R *(( volatile unsigned long*) 0x4002551C)
#define GPIO_PORTF_PUR_R *(( volatile unsigned long*) 0x40025510)
#define GPIO_PORTF_CR_R *((  volatile unsigned long*) 0x40025524)
#define GPIO_PORTF_LOCK_R *((  volatile unsigned long*) 0x40025520)
#define GPIO_PORTF_AFSEL_R *((  volatile unsigned long*) 0x40025420)

#define GPIO_PORTE_DIR_R *(( volatile unsigned long*) 0x4005C400)
#define GPIO_PORTE_DEN_R *(( volatile unsigned long*) 0x4005C51C)
#define GPIO_PORTE_AFSEL (*((volatile unsigned long*) 0x4005C420))
#define GPIO_PORTE_AMSEL (*((volatile unsigned long*) 0x4005C528))

#define ADC1_ACTSS_R (*((volatile unsigned long*) 0x40039000))
#define ADC1_EMUX_R (*((volatile unsigned long*) 0x40039014))
#define ADC1_SSMUX3_R (*((volatile unsigned long*) 0x400390A0)
#define ADC1_SSCTL3_R (*((volatile unsigned long*) 0x400390A4)
#define ADC1_IM_R (*((volatile unsigned long*) 0x40039008))
#define ADC1_ISC_R (*((volatile unsigned long*) 0x4003900C))
#define ADC1_SSFIFO3_R (*((volatile unsigned long*) 0x400390A8))

#define SYSTEM_CLOCK_FREQUENCY 80000000
#define DELAY_VALUE 4000000   
#define GPIO_PORTF_PIN3 0x08 //GREEN
#define GPIO_PORTF_PIN2 0x04 //BLUE
#define GPIO_PORTF_PIN1 0x02 //RED
#define GPIO_PORTF_PIN0 0x01 //sw1
#define GPIO_PORTF_PIN5 0x10 //sw2

#define GPTMCTL_TIMER0 (*(( volatile unsigned long*) 0x4003000C))
#define GPTMCFG_TIMER0 (*(( volatile unsigned long*) 0x40030000))
#define GPTMTAMR_TIMER0 (*(( volatile unsigned long*) 0x40030004))
#define GPTMTAILR_TIMER0 (*(( volatile unsigned long*) 0x40030028))
#define GPTMRIS_TIMER0 (*(( volatile unsigned long*) 0x4003001C))
#define GPTMICR_TIMER0 (*(( volatile unsigned long*) 0x40030024))
#define GPTMIMR_TIMER0 (*(( volatile unsigned long*) 0x40030018))

#define TIMER0_NVIC_ADD (*(( volatile unsigned long*) 0xE000E100))